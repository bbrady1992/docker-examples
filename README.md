# Introduction

A few small Docker examples for a work learning session.

1. **c++17-app** - A very simple C++ program that demonstrates how GCC can be run
   in a Docker container
2. **python-web-app** - Simple Flask web application that expands on Dockerfiles
   and demonstrates how to expose ports in running containers
3. **database-app** - Demonstrates how to persist data within containers using
   volumes. One container runs MySQL with the MySQL data directory mounted to
   a volume. A second container prints all of the rows in a table in the MySQL
   database and inserts additional rows. Subsequent runs of this application
   show that data persists between runs. Also demonstrates the basics of Docker
   Compose

Run the top-level init.sh script to build all of the example Docker images
at once.

```
./init.sh
```

