HERE=$(dirname $(readlink -f ${0}))

pushd ${HERE}/c++17-app/
./build.sh
popd

pushd ${HERE}/python-web-app/
./build.sh
popd

pushd ${HERE}/database-app/client
./build.sh
popd

pushd ${HERE}/database-app
./install-docker-compose.sh
popd

pushd ${HERE}/simple-container-example
./build.sh
popd

docker pull mysql:8
