# This terrible, terrible Python program was only written to demonstrate
# the usage of Docker volumes in the context of persisting MySQL DB
# data. Forgive its hideousness.
import mysql.connector
from time import sleep
from mysql.connector import errorcode
from random import randrange

sleep(20)
try:
  cnx = mysql.connector.connect(
          user='root', 
          password='rootpassword',
          host='db',
          database='exampledb')
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
else:
  print("Connected to DB")

  example_table = (
    "CREATE TABLE `words` ("
    "  `word_id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `word`  varchar(255) NOT NULL,"
    "  PRIMARY KEY (`word_id`)"
    ");")
  cursor = cnx.cursor()
  try:
      print("Creating table 'words'")
      cursor.execute(example_table)
  except mysql.connector.Error as err:
      if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
          print("already exists.")
      else:
          print(err.msg)
  else:
      print("OK")

  random_words = ["EdwinTheTermin(al)ator", "CaishMunney", "PoisonIvy", "Termite", "WhatEverHappenedToStaticShock?", "IveNeverSeenTheHolyGrailAndNowImTooAfraidToTellPeople"]
  print("Inserting three random words into the database...")
  for i in range(3):
      idx = randrange(len(random_words))
      insert = ('INSERT INTO words (word) VALUES (%s)')
      word_to_insert = (random_words[idx],)
      cursor.execute(insert, word_to_insert)
      cnx.commit()

  print("Printing all words inserted into DB so far")
  cursor.execute("SELECT word_id, word from words")
  for (word_id, word) in cursor:
      print("{}: {}".format(word_id, word))

  cursor.close()
  cnx.close()
