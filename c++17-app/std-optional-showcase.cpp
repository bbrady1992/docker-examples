#include <optional>
#include <iostream>
#include <string>

std::optional<std::string> getOptionalString(int32_t n) {
  if (n % 2 == 0) {
    return "This C++ program uses the std::optional class introduced in C++17";
  } else {
    return std::nullopt;
  }
}

int main() {
  auto optional1 = getOptionalString(2);
  if (optional1) {
    std::cout << optional1.value() << "\n";
  }
}
