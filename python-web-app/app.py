from flask import Flask, send_file
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Welcome! This site is running inside a Docker container'

@app.route('/calvin')
def show_image():
    return send_file('images/calvin-hammer.gif', mimetype='image/gif')
